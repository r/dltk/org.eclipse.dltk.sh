/*******************************************************************************
 * Copyright (c) 2009-2011 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite for all org.eclipse.dltk.sh.ui.text tests.
 * 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ AssignmentRuleTest.class, DollarBraceCountingRuleTest.class, DollarRuleTest.class,
		DoubleQuoteScannerTest.class, EvalScannerTest.class })
public class TextSuite {
}
