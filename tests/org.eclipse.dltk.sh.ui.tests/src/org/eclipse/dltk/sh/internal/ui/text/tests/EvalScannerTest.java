/*******************************************************************************
 * Copyright (c) 2010 Mat Booth and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.dltk.sh.internal.ui.Activator;
import org.eclipse.dltk.sh.internal.ui.IShellColorConstants;
import org.eclipse.dltk.sh.internal.ui.text.EvalScanner;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.junit.Test;

/**
 * Test the scanner for command substitution partitions.
 */
public class EvalScannerTest extends AbstractScannerTester {

	@Override
	protected RuleBasedScanner getScanner() {
		return new EvalScanner(cm, Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected String getText() {
		return "$vars in command ${substitutions}";
	}

	/**
	 * Tests that we correctly highlight $dollar style parameter expansions
	 * within command substitutions.
	 */
	@Test
	public void testDollar() {
		IToken token = getNextToken();
		assertEquals(5, scanner.getTokenLength());
		assertEquals(0, scanner.getTokenOffset());
		TextAttribute ta = (TextAttribute) token.getData();
		assertEquals(ta.getForeground(), cm.getColor(IShellColorConstants.SHELL_VARIABLE));
	}

	/**
	 * Tests that we correctly highlight ${dollar-brace} style parameter
	 * expansions within command substitutions.
	 */
	@Test
	public void testDollarBrace() {
		IToken token = getNthToken(14);
		assertEquals(16, scanner.getTokenLength());
		assertEquals(17, scanner.getTokenOffset());
		TextAttribute ta = (TextAttribute) token.getData();
		assertEquals(ta.getForeground(), cm.getColor(IShellColorConstants.SHELL_VARIABLE));
	}

	/**
	 * Everything else should have the default highlighting for this partition
	 * type.
	 */
	@Test
	public void testDefault() {
		IToken token = getNthToken(3);
		assertEquals(1, scanner.getTokenLength());
		assertEquals(6, scanner.getTokenOffset());
		TextAttribute ta = (TextAttribute) token.getData();
		assertEquals(ta.getForeground(), cm.getColor(IShellColorConstants.SHELL_EVAL));
	}
}
