/*******************************************************************************
 * Copyright (c) 2010 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.core.parser.tests;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class ShellScriptSourceParserTest {

	@Test @Ignore
	public void testParse() {
		fail("Not yet implemented");
	}

}
