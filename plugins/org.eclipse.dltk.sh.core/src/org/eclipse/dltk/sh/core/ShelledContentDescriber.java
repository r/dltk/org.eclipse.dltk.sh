/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.core;

import java.util.regex.Pattern;

import org.eclipse.dltk.core.ScriptContentDescriber;

public class ShelledContentDescriber extends ScriptContentDescriber {

	protected static final Pattern[] HEADER_PATTERNS = { Pattern.compile("^#!.*sh.*", Pattern.MULTILINE) };

	@Override
	protected Pattern[] getHeaderPatterns() {
		return HEADER_PATTERNS;
	}
}
