/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui;

import org.eclipse.dltk.core.IDLTKLanguageToolkit;
import org.eclipse.dltk.sh.core.ShellScriptLanguageToolkit;
import org.eclipse.dltk.sh.internal.ui.interpreter.ShellInterpreterPreferencePage;
import org.eclipse.dltk.ui.AbstractDLTKUILanguageToolkit;
import org.eclipse.jface.preference.IPreferenceStore;

public class ShelledUILanguageToolkit extends AbstractDLTKUILanguageToolkit {
	@Override
	public IDLTKLanguageToolkit getCoreToolkit() {
		return ShellScriptLanguageToolkit.getDefault();
	}

	@Override
	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	@Override
	public String getInterpreterPreferencePage() {
		return ShellInterpreterPreferencePage.PAGE_ID;
	}
}
