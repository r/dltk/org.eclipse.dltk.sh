/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.search;

import org.eclipse.dltk.ast.ASTNode;
import org.eclipse.dltk.core.search.matching.MatchLocator;
import org.eclipse.dltk.core.search.matching.MatchLocatorParser;
import org.eclipse.dltk.core.search.matching.PatternLocator;

public class ShelledMatchLocationParser extends MatchLocatorParser {
	protected ShelledMatchLocationParser(MatchLocator locator) {
		super(locator);
	}

	@Override
	protected void processStatement(ASTNode node, PatternLocator locator) {
		super.processStatement(node, locator);
	}
}
