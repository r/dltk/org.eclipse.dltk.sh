/*******************************************************************************
 * Copyright (c) 2012 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text.folding;

import org.eclipse.dltk.ui.text.folding.IFoldingBlockKind;

public enum FoldingBlockKind implements IFoldingBlockKind {
	FUNCTION, COMMENT;

	@Override
	public boolean isComment() {
		return this == COMMENT;
	}
}