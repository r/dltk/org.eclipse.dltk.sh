/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.editor;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.dltk.sh.internal.ui.Activator;
import org.eclipse.dltk.sh.internal.ui.text.IShellPartitions;
import org.eclipse.dltk.sh.internal.ui.text.ShellTextTools;
import org.eclipse.jface.text.IDocument;

public class ShellDocumentSetupParticipant implements IDocumentSetupParticipant {

	@Override
	public void setup(IDocument document) {
		ShellTextTools tools = Activator.getDefault().getTextTools();
		tools.setupDocumentPartitioner(document, IShellPartitions.SHELL_PARTITIONING);
	}
}
