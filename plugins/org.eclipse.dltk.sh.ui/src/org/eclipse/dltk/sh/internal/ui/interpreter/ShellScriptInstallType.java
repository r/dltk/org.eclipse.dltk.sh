/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.interpreter;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.dltk.core.environment.IDeployment;
import org.eclipse.dltk.core.environment.IFileHandle;
import org.eclipse.dltk.internal.launching.AbstractInterpreterInstallType;
import org.eclipse.dltk.launching.EnvironmentVariable;
import org.eclipse.dltk.launching.IInterpreterInstall;
import org.eclipse.dltk.launching.LibraryLocation;
import org.eclipse.dltk.sh.core.ShelledNature;
import org.eclipse.dltk.sh.internal.ui.Activator;

public class ShellScriptInstallType extends AbstractInterpreterInstallType {
	private static final String[] INTERPRETER_NAMES = { "sh", "bash", "zsh", "dash", "ksh", "tcsh", "csh" };

	@Override
	protected IPath createPathFile(IDeployment deployment) {
		return null;
	}

	@Override
	public synchronized LibraryLocation[] getDefaultLibraryLocations(IFileHandle installLocation,
			EnvironmentVariable[] variables, IProgressMonitor monitor) {
		return new LibraryLocation[0];
	}

	@Override
	protected IInterpreterInstall doCreateInterpreterInstall(String id) {
		return new ShellScriptInstall(this, id);
	}

	@Override
	protected ILog getLog() {
		return Activator.getDefault().getLog();
	}

	@Override
	public String getName() {
		return "Shell Interpreter";
	}

	@Override
	public String getNatureId() {
		return ShelledNature.SHELLED_NATURE;
	}

	@Override
	protected String getPluginId() {
		return Activator.PLUGIN_ID;
	}

	@Override
	protected String[] getPossibleInterpreterNames() {
		return INTERPRETER_NAMES;
	}

}
