/*******************************************************************************
 * Copyright (c) 2009, 2017 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.selection;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.dltk.core.IMember;
import org.eclipse.dltk.ui.documentation.IScriptDocumentationProvider;
import org.eclipse.linuxtools.man.parser.ManPage;

public class ShellDocumentationProvider implements IScriptDocumentationProvider {

	@Override
	public Reader getInfo(String content) {
		String helpInfo = getHelp(content);
		if (!helpInfo.isEmpty()) {
			return new StringReader("<pre>" + helpInfo + "</pre>");
		}
		StringBuilder manInfo = new ManPage(content).getStrippedPage();
		if (manInfo.length() == 0) {
			return null;
		}
		// Any more than ~10kb and the hover help starts to take a really long time to
		// render, so truncate it in that case and advise to use the man page view
		final int maxLen = 1024 * 10;
		StringBuilder sb = new StringBuilder("<pre>");
		if (manInfo.length() > maxLen) {
			sb.append(manInfo.substring(0, maxLen));
		} else {
			sb.append(manInfo);
		}
		sb.append("</pre>");
		if (manInfo.length() > maxLen) {
			sb.append(
					"<p>This man page is truncated, use <b>Show Man Page</b> (Alt+M) to see the whole page for the selected word.</p>");
		}
		return new StringReader(sb.toString());
	}

	private static String getHelp(String command) {
		List<String> commands = new ArrayList<>();
		commands.add("bash");
		commands.add("-c");
		commands.add("help " + command);
		ProcessBuilder process = new ProcessBuilder(commands);
		final StringBuilder out = new StringBuilder();
		try (InputStream stream = process.start().getInputStream()) {
			final char[] buffer = new char[1024];
			try (Reader in = new InputStreamReader(stream)) {
				for (;;) {
					int rsz = in.read(buffer, 0, buffer.length);
					if (rsz < 0) {
						break;
					}
					out.append(buffer, 0, rsz);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out.toString();
	}

	@Override
	public Reader getInfo(IMember element, boolean lookIntoParents, boolean lookIntoExternal) {
		// TODO show comments for functions if there is any
		return null;
	}

}
