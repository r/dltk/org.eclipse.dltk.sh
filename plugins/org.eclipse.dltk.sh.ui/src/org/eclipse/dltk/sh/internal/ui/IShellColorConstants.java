/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui;

import org.eclipse.dltk.ui.text.DLTKColorConstants;

public interface IShellColorConstants {
	String SHELL_COMMENT = DLTKColorConstants.DLTK_SINGLE_LINE_COMMENT;
	String SHELL_DEFAULT = DLTKColorConstants.DLTK_DEFAULT;
	String SHELL_DOUBLE_QUOTE = "SHELLED_interpolated_string";
	String SHELL_EVAL = DLTKColorConstants.DLTK_OPERATOR;
	String SHELL_FUNCTION = DLTKColorConstants.DLTK_FUNCTION_DEFINITION;
	String SHELL_HASHBANG = DLTKColorConstants.DLTK_MULTI_LINE_COMMENT;
	String SHELL_KEYWORD = DLTKColorConstants.DLTK_KEYWORD;
	String SHELL_SINGLE_QUOTE = DLTKColorConstants.DLTK_STRING;
	String SHELL_VARIABLE = DLTKColorConstants.DLTK_ARGUMENT;
	String SHELL_COMMAND = DLTKColorConstants.DLTK_BASE_CLASS;
	String SHELL_TODO_TAG = DLTKColorConstants.TASK_TAG;
}