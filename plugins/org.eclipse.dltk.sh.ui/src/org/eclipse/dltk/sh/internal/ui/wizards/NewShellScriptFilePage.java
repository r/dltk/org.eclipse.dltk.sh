/*******************************************************************************
 * Copyright (c) 2015 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.wizards;

import org.eclipse.dltk.ui.wizards.NewSourceModulePage;

public class NewShellScriptFilePage extends NewSourceModulePage {

	@Override
	protected String getPageTitle() {
		return "Shell Script";
	}

	@Override
	protected String getPageDescription() {
		return "Create a new Shell Script.";
	}

	@Override
	protected String getRequiredNature() {
		return null;
	}
}
