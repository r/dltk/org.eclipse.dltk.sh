/*******************************************************************************
 * Copyright (c) 2011 Mat Booth and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Mat Booth
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text;

import org.eclipse.jface.text.rules.IWordDetector;

/**
 * Determines whether given any given character forms part of a shell
 * assignment.
 */
public class AssignmentDetector implements IWordDetector {

	@Override
	public boolean isWordPart(char c) {
		return Character.isJavaIdentifierPart(c) || (c == '[') || (c == ']');
	}

	@Override
	public boolean isWordStart(char c) {
		return Character.isJavaIdentifierStart(c);
	}

}
