/*******************************************************************************
 * Copyright (c) 2014 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Peter Palaga - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text.folding;

import org.eclipse.dltk.ui.preferences.OverlayPreferenceStore;
import org.eclipse.dltk.ui.text.folding.SourceCodeFoldingPreferenceBlock;
import org.eclipse.jface.preference.PreferencePage;

/**
 * A preference block for folding of Shell Script commands.
 */
public class ShellCodeFoldingPreferenceBlock extends SourceCodeFoldingPreferenceBlock {
	public ShellCodeFoldingPreferenceBlock(OverlayPreferenceStore store, PreferencePage page) {
		super(store, page);
	}

	@Override
	protected String getInitiallyFoldMethodsText() {
		return "Functions";
	}

	@Override
	protected boolean supportsClassFolding() {
		return false;
	}

}
