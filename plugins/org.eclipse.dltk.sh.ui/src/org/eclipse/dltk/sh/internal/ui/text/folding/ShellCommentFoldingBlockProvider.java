/*******************************************************************************
 * Copyright (c) 2012 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text.folding;

import org.eclipse.dltk.sh.internal.ui.Activator;
import org.eclipse.dltk.sh.internal.ui.text.IShellPartitions;
import org.eclipse.dltk.ui.text.folding.IFoldingContent;
import org.eclipse.dltk.ui.text.folding.PartitioningFoldingBlockProvider;

public class ShellCommentFoldingBlockProvider extends PartitioningFoldingBlockProvider {

	public ShellCommentFoldingBlockProvider() {
		super(Activator.getDefault().getTextTools());
	}

	@Override
	public void computeFoldableBlocks(IFoldingContent content) {
		if (isFoldingComments()) {
			computeBlocksForPartitionType(content, IShellPartitions.COMMENT_CONTENT_TYPE, FoldingBlockKind.COMMENT,
					isCollapseComments());
		}
	}

}
