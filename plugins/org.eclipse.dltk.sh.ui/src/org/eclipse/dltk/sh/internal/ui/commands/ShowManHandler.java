/*******************************************************************************
 * Copyright (c) 2009, 2017 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.linuxtools.man.views.ManView;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * A command handler that opens the Man Page view and tries to show the man page
 * for whatever text is currently highlighted.
 */
public class ShowManHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) {
		try {
			ISelection selection = HandlerUtil.getCurrentSelection(event);
			String manPage = "";
			if (selection instanceof TextSelection) {
				TextSelection textSelection = (TextSelection) selection;
				manPage = textSelection.getText();
			}

			if (!manPage.isEmpty()) {
				ManView view = (ManView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage()
						.showView(ManView.ID);
				view.setManPageName(manPage);
			}
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}
}
