/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text;

/**
 * A type of indent.
 */
public final class IndentType {
	/** A single indent decrement */
	public static final IndentType DECREMENT = new IndentType();
	/** A single indent increment */
	public static final IndentType INCREMENT = new IndentType();
	/** An inflexion - both an increment and a decrement */
	public static final IndentType INFLEXION = new IndentType();

	private IndentType() {
	}
}
