/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.text;

import org.eclipse.jface.text.IDocument;

public interface IShellPartitions {
	String SHELL_PARTITIONING = "__shell_partitioning";

	// Content types supplied by the shell script partitioner
	String COMMENT_CONTENT_TYPE = "__comment";
	String DOUBLE_QUOTE_CONTENT_TYPE = "__double_quote";
	String PARAM_CONTENT_TYPE = "__parameter";
	String EVAL_CONTENT_TYPE = "__eval";
	String HASHBANG_CONTENT_TYPE = "__hashbang";
	String FUNCTION_CONTENT_TYPE = "__function";
	String SINGLE_QUOTE_CONTENT_TYPE = "__single_quote";

	String[] CONTENT_TYPES = new String[] { IDocument.DEFAULT_CONTENT_TYPE, HASHBANG_CONTENT_TYPE, COMMENT_CONTENT_TYPE,
			SINGLE_QUOTE_CONTENT_TYPE, DOUBLE_QUOTE_CONTENT_TYPE, PARAM_CONTENT_TYPE, EVAL_CONTENT_TYPE,
			FUNCTION_CONTENT_TYPE };
}