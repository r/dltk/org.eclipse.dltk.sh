/*******************************************************************************
 * Copyright (c) 2009 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.interpreter;

import org.eclipse.dltk.internal.debug.ui.interpreters.InterpretersBlock;
import org.eclipse.dltk.internal.debug.ui.interpreters.ScriptInterpreterPreferencePage;

public class ShellInterpreterPreferencePage extends ScriptInterpreterPreferencePage {

	public static final String PAGE_ID = "org.eclipse.dltk.sh.ui.preferences.interpreters";

	@Override
	public InterpretersBlock createInterpretersBlock() {
		return new ShellScriptInterpretersBlock();
	}
}
