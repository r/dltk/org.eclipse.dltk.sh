/*******************************************************************************
 * Copyright (c) 2015 Red Hat Inc. and others.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Alexander Kurtakov - initial API and implementation
 *******************************************************************************/
package org.eclipse.dltk.sh.internal.ui.wizards;

import org.eclipse.dltk.sh.core.ShelledNature;
import org.eclipse.dltk.ui.DLTKUIPlugin;
import org.eclipse.dltk.ui.wizards.ProjectWizard;
import org.eclipse.dltk.ui.wizards.ProjectWizardFirstPage;
import org.eclipse.dltk.ui.wizards.ProjectWizardSecondPage;

public class ShellScriptProjectWizard extends ProjectWizard {

	public ShellScriptProjectWizard() {
		setWindowTitle("New Shell Script Project");
		setDialogSettings(DLTKUIPlugin.getDefault().getDialogSettings());
	}

	@Override
	public String getScriptNature() {
		return ShelledNature.SHELLED_NATURE;
	}

	@Override
	public void addPages() {
		super.addPages();
		ProjectWizardFirstPage firstPage = new ProjectWizardFirstPage() {

			@Override
			protected boolean interpeterRequired() {
				return false;
			}
		};

		// First page
		firstPage.setTitle("Shell Script Project");
		firstPage.setDescription("Create a new Shell Script project.");
		addPage(firstPage);

		// Second page
		ProjectWizardSecondPage secondPage = new ProjectWizardSecondPage(firstPage);
		addPage(secondPage);
	}
}
